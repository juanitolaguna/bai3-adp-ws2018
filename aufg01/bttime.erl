-module(bttime).
-import(btree, [insertList/2]).
-export([insert/1, delete/2, find/2, equal/1]).

insert(N) ->
  List = util:randomliste(N),
  Start = erlang:timestamp(),
  btree:insertList({}, List),
  Stop = erlang:timestamp(),
  MS = util:float_to_int(timer:now_diff(Stop, Start) / 1000),
  file:write_file('insert.log', io_lib:fwrite("~p,~p\n", [N, MS]), [append]).

delete(T, N) ->
  List = util:randomliste(N),
  Start = erlang:timestamp(),
  btree:deleteList(T, List),
  Stop = erlang:timestamp(),
  MS = util:float_to_int(timer:now_diff(Stop, Start) / 1000),
  file:write_file('delete.log', io_lib:fwrite("~p,~p\n", [N, MS]), [append]).

find(T, N) ->
  List = util:randomliste(N),
  Start = erlang:timestamp(),
  btree:findList(T, List),
  Stop = erlang:timestamp(),
  MS = util:float_to_int(timer:now_diff(Stop, Start) / 1000),
  file:write_file('find.log', io_lib:fwrite("~p,~p\n", [N, MS]), [append]).

equal(N) ->
  Tree = btree:makeTree(util:randomliste(N)),
  Start = erlang:timestamp(),
  btree:equalBT(Tree, Tree),
  Stop = erlang:timestamp(),
  MS = util:float_to_int(timer:now_diff(Stop, Start) / 1000),
  file:write_file('equal.log', io_lib:fwrite("~p,~p\n", [N, MS]), [append]).

-module(btree).
-export([initBT/0, isEmptyBT/1, equalBT/2, insertBT/2, deleteBT/2, findBT/2, inOrderBT/1, insertList/2, deleteList/2, findList/2, makeTree/1, map/2]).
-define(EMPTY, {}).

% Gibt einen leeren Baum zurück.
initBT() -> ?EMPTY.

% Gibt zurück ob ein Baum T leer ist.
isEmptyBT(T) -> equalBT(T, ?EMPTY).

% Gibt zurück ob zwei Bäume semantisch gleich sind.
equalBT({}, {}) -> true;
equalBT({}, _B) -> false;
equalBT(_A, {}) -> false;
equalBT(A, B) -> equalList(inOrderBT(A), inOrderBT(B)).

% Gibt zurück ob zwei Listen gleich sind.
equalList([], []) -> true;
equalList(_A, []) -> false;
equalList([], _B) -> false;
equalList([A | AR], [B | BR]) ->
  if
    A == B -> equalList(AR, BR);
    true -> false
  end.

% Fügt einem Baum T ein Element E hinzu.
insertBT({}, E) -> { E, 1, ?EMPTY, ?EMPTY };
insertBT(T = { C, _H, L, R }, E) ->
  if
    C > E ->
      NL = insertBT(L, E),
      { C, max(heightBT(NL), heightBT(R)) + 1, NL, R };
    C < E ->
      NR = insertBT(R, E),
      { C, max(heightBT(L), heightBT(NR)) + 1, L, NR };
    C == E -> T
  end.

% Gibt die Höhe eines Baumes zurück.
heightBT({}) -> 0;
heightBT({ _C, H, _L, _R }) -> H.

% Entfernt ein Element E von einem Baum T.
deleteBT({}, _E) -> {};
deleteBT({ C, _H, {}, {} }, E) when C == E -> {};
deleteBT({ C, _H, L, {} }, E) when C == E -> L;
deleteBT({ C, _H, {}, R }, E) when C == E -> R;
deleteBT({ C, _H, L, R }, E) ->
  if
    E < C ->
      NL = deleteBT(L, E),
      { C, max(heightBT(NL), heightBT(R)) + 1, NL, R };
    E > C ->
      NR = deleteBT(R, E),
      { C, max(heightBT(L), heightBT(NR)) + 1, L, NR };
    E == C -> insertList(R, inOrderBT(L))
  end.

% Fügt einem Baum T alle Elemente einer Liste hinzu.
insertList(T, []) -> T;
insertList(T, [H | R]) -> insertList(insertBT(T, H), R).

% Entfernt von einem Baum T alle Elemente einer Liste.
deleteList(T, []) -> T;
deleteList(T, [H | R]) -> deleteList(deleteBT(T, H), R).

% Gibt die Höhe eines Elements E zurück.
% Gibt 0 zurück falls es E nicht gibt.
findBT({}, _E) -> 0;
findBT({ C, H, L, R }, E) ->
  if
    E > C -> findBT(R, E);
    E < C -> findBT(L, E);
    E == C -> H
  end.

% Gibt einen Baum T als geordnete Liste zurück.
inOrderBT({}) -> [];
inOrderBT(T) -> inOrderBT(T, []).

% Hilfsfunktion für inOrderBT/1.
inOrderBT({}, A) -> A;
inOrderBT({ C, _H, L, R }, A) -> inOrderBT(L, [ C | inOrderBT(R, A) ]).

% Hilfsfunktion für um findBT und equalBT zu testen.
makeTree(Liste) -> insertList({}, Liste).

map(_F,_Tree, []) -> [];
<<<<<<< HEAD
map(F, Tree, [H | T]) -> [F(Tree, H)|map(F, Tree, T)].

map(_, []) -> [];
map(F, [H | T]) -> [F(H) | map(F,T)].

% Test für findBT
findListTest(Tree, List) ->
  map(fun findBT/2, Tree, List).
=======
map(F, Tree, [H | T]) -> [F(Tree, H) | map(F, Tree, T)].

map(_, []) -> [];
map(F, [H | T]) -> [F(H) | map(F, T)].

%Test für findBT
findList(Tree, List) -> map(fun findBT/2, Tree, List).
>>>>>>> 9423558f29ef476995d74329300015e8ce7d18d7

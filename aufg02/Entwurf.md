# Entwurf A2


## insertionsort


## quicksort - Sortiert eine Liste nach dem quicksort Algorithmus
#### Schnittstelle : qsort(pivot-methode, Liste, switch-number)
##### Parameter:
- Liste: enthält zu sortierende Zahlen
- Pivot-Methode:
  - left - das erste Element in der Liste wird immer als Pivot Element gewählt.
  - middle - das mittlere Element in der Liste wird immer als Pivot Element gewählt.
  - right - das letzte Element in der Liste wird immer als Pivot Element gewählt.
  - median -  das Element, welches dem Wert nach in der Mitte liegt (von left, middle, right) wird immer Ausgewählt.
- Switch-Number:
  - Enthält die Länge N der Teilliste.
  - Ab dieser Länge N wird weiter mit InsertionSort sortiert.

##### Rückgabewert - eine korrekt sortierte Liste wird zurückgegeben.
#### Ablauf:


Ohne SwitchNumber:
- Ist die Liste leer, so gebe auch eine leere Liste zurück
 - Wähle als Pivot:
    - das erste oder letzte oder mittlere falls Pivot-Methode "left", "middle", "right"
    - oder von den oberen drei das Element mit den Mittleren Wert falls Pivot-Methode "median"
    - oder von eine zufälliges Element aus der Liste falls Pivot-Methode "random"

 - Teile die restliche Liste (ohne Pivot) in zwei Listen auf:
  - "ListeKleiner" - alle Elemente < Pivot
  - "ListeGrößer" - alle Elemente > Pivot

- Konkatenniere den rekursiven Aufruf der Methode auf die "ListeKleiner"
  - mit Pivot und
  - mit rekursiven Aufruf der Methode auf die "ListeGrößer"

Mit SwitchNumber:
  - Test am anfang und vor jedem rekursiven Aufruf ob länge der Liste > SwitchNumber.
  - Wenn nicht, dann führe "insertionS" auf die mitgegebene Liste aus.





## heapsort
Beim Heapsort bilden wir eine ungeordnete Liste auf eine Heap-Datenstruktur ab.
Ein Heap ist eine Baumstrucktur welche aus einem Knoten und Blättern besteht,
welche wiederum ein Knoten mit Blättern sein können
Die rep. invariante vom Heap ist das der Key vom parentNode >= childNode ist.
Auch umgekehrt möglch: parentNode =< childNode.


Die Fundamentale Operation beim HeapSort nennen wir "max_heapify".
"max_heapify" korrigiert eine einzelne Verletzung der Heap Eigenschaft:
- überprüfe ob der Wert vom parentNode >= childNode.
  - falls nicht, vertausche die Werte vom parentNode <-> childNode.


Erstelle einen Max-Heap aus einer ungeordneten Liste:
Eine BaumStruktur kann in einer Liste wir folgt dargestellt werden:
parentNode = Wert über den index i zugreifbar
childLeft = 2*i
childRight = 2*i+1

Wenn wir so eine Liste betrachten, dann besteht die zweite hälfte der Liste nur aus Blätter.
Es hatt keinen Sinn "max_heapify" auf die Blätter aufzurufen da die sie Per definition jeweils ein maxHeap sind.

1. stelle maxHeap eigenschaft des Baums über max_heapify her.
2. extrahiere root und füge in in eine neue Liste hinzu.
3. setzte an die Stelle des roots das kleinste element im Heap.
4. stelle wieder maxHeap Eigenschaft über max_heapify her.
5. fange wieder bei Schritt 2 an, bis der Heap leer ist.



### Erwartete Ergebnisse (beenden!)

Heapsort:
Betrachten wir „max_heapify“ :
Auf min_höhe-1 beträgt die Laufzeit jeweils O(1) und O(H) für knoten der höhe H über den Blättern.
In Worst-Case muss jeder parent mit den childNodes vertauscht werden.

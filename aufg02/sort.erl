-module(sort).
-compile(export_all).




% ===================================================
% InsertionSort
% ===================================================
insertionS([]) -> [];
insertionS([ H | T ]) -> insertList(T, [ H ]).

insertList([], S) -> S;
insertList([ H | T ], S) -> insertList(T, insert(H, S)).

% Fügt ein Element E einer Liste L an korrekt sortierter Position hinzu.
% Die Liste L muss dafür bereits sortiert sein.
insert(E, []) -> [ E ];
insert(E, [ H | T ]) when E > H -> [ H | insert(E, T)];
insert(E, S) -> [ E | S ].





% ===================================================
% Quicksort
% - median muss noch implementiert werden
% - random muss implementiert werden
% ===================================================

% ---------------------------------------------------
% Quicksort einfach.
% ---------------------------------------------------
% quicksort([]) -> [];
% quicksort([Pivot|Rest]) ->
% 	{Smaller, Larger} = partition(Pivot, Rest, [],[]),
% 	quicksort(Smaller) ++ [Pivot] ++ quicksort(Larger).
% ---------------------------------------------------

qsort3(PivotMethod, List, SwitchNumber) ->
	ListLength = lists:flatlength(List),
	qsort(PivotMethod, List, SwitchNumber, ListLength).

qsort(_PivotMethod, List, SwitchNumber, ListLength) when ListLength < SwitchNumber ->
	insertionS(List);

qsort(left, [], _SwitchNumber, _ListLength) -> [];
qsort(left, [Pivot|Rest], SwitchNumber, _ListLength) ->
	{Smaller, Larger} = partition(Pivot, Rest, [],[]),
	SmallerLength = lists:flatlength(Smaller),
	LargerLength = lists:flatlength(Larger),
	qsort(left, Smaller, SwitchNumber, SmallerLength) ++ [Pivot] ++ qsort(left, Larger, SwitchNumber, LargerLength);

qsort(middle, [], _SwitchNumber, _ListLength) -> [];
qsort(middle, List, SwitchNumber, _ListLength ) ->
	{SubList, [Pivot|SubList2]} = helpers:getMiddle(List),
	NewList = helpers:concat(SubList, SubList2),
	{Smaller, Larger} = partition(Pivot, NewList, [],[]),
	SmallerLength = lists:flatlength(Smaller),
	LargerLength = lists:flatlength(Larger),
	qsort(middle, Smaller, SwitchNumber, SmallerLength) ++ [Pivot] ++ qsort(middle, Larger, SwitchNumber, LargerLength);


qsort(right, [], _SwitchNumber, _ListLength) -> [];
qsort(right, List, SwitchNumber, _ListLength) ->
	{SubList, [Pivot]} = helpers:getRight(List),
	{Smaller, Larger} = partition(Pivot, SubList, [],[]),
	SmallerLength = lists:flatlength(Smaller),
	LargerLength = lists:flatlength(Larger),
	qsort(right, Smaller, SwitchNumber, SmallerLength) ++ [Pivot] ++ qsort(right, Larger, SwitchNumber, LargerLength).
%
%
% qsort(median, [], SwitchNumber) -> [];
% qsort(median, List, SwitchNumber) ->
% 	{Smaller, [Pivot|Larger]} = partition(helpers:getMedian(List), List, [],[]),
% 	qsort(median, Smaller, SwitchNumber) ++ [Pivot] ++ qsort(median, Larger, SwitchNumber).

% ---------------------------------------------------
% Helper für Quicksort
% ---------------------------------------------------
partition(_,[],Smaller, Larger) -> {Smaller, Larger};
partition(Pivot, [H|T], Smaller, Larger) ->
	if
		H =< Pivot -> partition(Pivot, T, [H|Smaller], Larger);
		H > Pivot -> partition(Pivot, T, Smaller, [H|Larger])
	end.
% ===================================================




% ===================================================
% HeapSort
% ===================================================


% Kodierung des Feldes: Nachfolger von Position i ist 2*i links und 2*i+1 rechts
% berechnet den Pfad zur ersten leeren Position
% l steht fuer links, r fuer rechts
% Beispiel: sort:calcPath(1). --> []
% 		sort:calcPath(2). --> [l]
% 		sort:calcPath(3). --> [r]
% 		sort:calcPath(4). --> [l,l]
% 		sort:calcPath(5). --> [l,r]
% 		sort:calcPath(6). --> [r,l]
% 		sort:calcPath(7). --> [r,r]
calcPath(Number) -> calcPath(Number,[]).
% aktuelle Position ist Wurzel
calcPath(1,Accu) -> Accu;
% aktuelle Position ist gerade
calcPath(Number,Accu) when Number rem 2 =:= 0 ->
	calcPath(Number div 2,[l|Accu]);
% aktuelle Position ist ungerade
calcPath(Number,Accu) when Number rem 2 =/= 0 ->
	calcPath((Number-1) div 2,[r|Accu]).


createHeap([]) -> {};
createHeap(List) ->
	createHeap_3(List, 0, {}).

createHeap_3([H|T], N, MaxHeap) ->
	Path = calcPath(N),
	NewHeap = insertElement(MaxHeap, H, Path),
	createHeap_3(T, N + 1, NewHeap).




insertElement(H, E, []) -> 
io:format("here!\n"),
H;
insertElement({V, L, R}, E, Path) when E > V ->
  		insertElement({E, L, R}, V, Path);
insertElement({V, L, R}, E, [H|T]) ->
		if
			H == l ->
				NewLeft = insertElement(L, E, T),
				{V, NewLeft, R};
			H == r ->
				NewRight = insertElement(R, E, T),
				{V, L, NewRight}
		end.

% List = [1,2,3,4,5,6,7,8].
% sort:createHeap(List).

% c(sort, debug_info).






% createHeap()



hsort([]) -> [].

pop([]) -> null.

getLast([]) -> null.

swap([]) -> null.

-module(helpers).
-compile(export_all).

% ===================================================
% Helper für Quicksort
% ===================================================


% ---------------------------------------------------
% Get pivot in the middle
% ---------------------------------------------------
getMiddle([]) -> null;
getMiddle(List) ->
	lists:split(lists:flatlength(List) div 2, List).

% ---------------------------------------------------
% Get left pivot
% ---------------------------------------------------
getLeft([]) -> null;
getLeft([H|_]) -> H.

% ---------------------------------------------------
% Get right pivot
% ---------------------------------------------------
getRight([]) -> null;
getRight(List) ->
  lists:split(lists:flatlength(List)-1, List).

% ---------------------------------------------------
% Get median pivot [min < median < max]
% ---------------------------------------------------
getMedian([]) -> null;
getMedian(List) ->
  Left = getLeft(List),
  Middle = getMiddle(List),
  Right = getRight(List),
  Pivot = lists:nth(2, lists:sort([Left, Middle, Right])),
	if
		Pivot == Left -> getLeft(List);
		Pivot == Right -> getRight(List);
		Pivot == Middle -> getMiddle(List)
	end.


% efficient concat
% more info : https://goo.gl/W2DxAQ

concat([], []) -> [];
concat(List1, []) -> List1;
concat([], List2) -> List2;
concat(List1, List2) ->
  concat2(lists:reverse(List1), List2).

concat2([], List2) -> List2;
concat2([H|T], List2) ->
  concat2(T, [H|List2]).

-module(heap).
-compile(export_all).

calcPath(Number) -> calcPath(Number,[]).
% aktuelle Position ist Wurzel
calcPath(1,Accu) -> Accu;
% aktuelle Position ist gerade
calcPath(Number,Accu) when Number rem 2 =:= 0 ->
	calcPath(Number div 2,[l|Accu]);
% aktuelle Position ist ungerade
calcPath(Number,Accu) when Number rem 2 =/= 0 ->
	calcPath((Number-1) div 2,[r|Accu]).


createHeap([]) -> {};
createHeap(List) ->
	createHeap_3(List, 0, {}).

createHeap_3([], _N, H) -> H;
createHeap_3([H|T], N, MaxHeap) ->
	Size = N + 1,
	Path = calcPath(Size),
	NewHeap = insertElement(MaxHeap, H, Path),
	createHeap_3(T, Size, NewHeap).

insertElement(H, E, []) -> 
	{E, {}, {}};
insertElement({V, L, R}, E, Path) when E >= V ->
  		insertElement({E, L, R}, V, Path);
insertElement({V, L, R}, E, List = [H|T]) ->
	if
		H == l ->
			NewLeft = insertElement(L, E, T),
			{V, NewLeft, R};
		H == r ->
			NewRight = insertElement(R, E, T),
			{V, L, NewRight}
	end.


% HeapLast is wrong

heapLast({V, {}, {}}, []) -> V;

heapLast({V, L, R}, [H|T]) -> 
	if
		H == l -> heapLast(L, T);
		H == r -> heapLast(R, T)	
	end.


heapify({H, {}, {}}) -> H;
heapify(Node = {H, {LV, _R, _L}, {}}) ->
	io:format("Node1: ~p \n", [Node]), 
	if
		LV > H -> {LV, {H, _R, _L}, {}};
		LV =< H -> Node 
	end;
heapify(Node = {H, {LV, LL, LR}, R = {RV, _, _}}) when LV > RV ->
	io:format("Node2: ~p\n", [Node]), 
	if
		LV > H -> {LV, heapify({H, LL, LR}), R};
		LV =< H -> Node
	end;
heapify(Node = {H, L = {LV, _, _}, {RV, RL, RR}}) when RV > LV ->
	io:format("Node3: ~p\n", [Node]), 
	if
		RV > H -> {RV, L, heapify({H, RL, RR})};
		RV =< H -> Node
	end.



heapS(List) -> 
	MaxHeap = createHeap(List),
	HeapSize = lists:flatlength(List),
	heapS2(MaxHeap, HeapSize, []).

heapS2({}, _HeapSize, Sorted) -> Sorted;

heapS2(Heap, HeapSize, Sorted) -> 
	Path = calcPath(HeapSize),
	MaxElem = heapLast(Heap, Path),
	NewHeap = insertTop(Heap, MaxElem),
	io:format("NewHeap: ~p\n", [NewHeap]),
	MaxHeap = heapify(NewHeap),
	io:format("Heap: ~p \n HeapSize: ~p \n Sorted: ~p\n",[Heap, HeapSize, Sorted]),
	heapS2(MaxHeap, HeapSize - 1, [MaxElem| Sorted]).


insertTop({}, E) -> {E, {}, {}};
insertTop({E, L, R}, NewElem) -> {NewElem, L, R}.











